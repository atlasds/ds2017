//TODO: Retirar disciplina da grade se não conseguir encaixar em uma turma


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//structs para simular as classes:
typedef struct aula{
	int dia;
	int horario;
}aula;

typedef struct turma{
	char sigla[3];
	struct aula* horarios;
}turma;

typedef struct materia{
	int turmaEscolhida;
	int prioridade;
	int nroTurmas;
	int creditos;
	char nome[30];
	struct turma* turmas;
}materia;

typedef struct grade{
	int* turmasEscolhidas;
}grade;

//Obj: ORGANIZAR A GRADE POR ORDEM CRESCENTE DE PRIORIDADE, PARA AS COM MAIOR PRIORIDADE SEREM ALOCADAS PRIMEIRO NA GRADE
//Método utilizado: bubble sort -> se utilizar algum método "dividir para conquistar" vai haver uma leve melhora no desempenho (mas com certeza será uma melhora imperceptivel)
void organizaMatPrioridade(materia* materias, int nroMaterias){
	materia temp;									//MATERIA TEMPORARIA PARA FAZER A TROCA
	int i, j;				
	for(i = 0; i < nroMaterias-1; i++){						//VAI DE 0 ATÉ NRO DE MATERIAS -1
		for(j = i+1; j < nroMaterias; j++){					//VAI DE I+1 ATÉ NRO DE MATERIAS
			if(materias[i].prioridade > materias[j].prioridade){		//SE A PRIORIDADE FOR MAIOR
				temp = materias[i];					//FAZ O BUBBLE SORT
				materias[i] = materias[j];
				materias[j] = temp;
			}
		}
	}
}
//já deve exister o método abaixo em alguma das classes
//obj: retorna 1 se há conflito de horario entre duas turmas, 0 se não há
int hasConflict(turma t1,int aulast1, turma t2,int aulast2){
	int i,j; //VARIAVEIS AUXILIARES
	for(i = 0; i < aulast1; i++){	//ITERA POR TODAS AS AULAS DA PRIMEIRA TURMA
		for(j = 0; j < aulast2; j++){//ITERA POR TODAS AS AULAS DA SEGUNDA TURMA
			if(t1.horarios[i].dia == t2.horarios[j].dia && t1.horarios[i].horario == t2.horarios[j].horario){//COMPARA SE AS AULAS OCORREM NO MSM HORARIO
				return 1;	//SE SIM, HÁ CONFLITO
			}
		}
	}
	return 0;//SE NÃO ENTRAR NO IF NENHUMA VEZ, NÃO HÁ CONFLITO
}
//o metodo abaixo deverá existir na classe grade (se ja tiver sido desenvolvida)
//obj: conferir se tem conflito entre a turma e as turmas ja existentes na grade
int checkGradeConflict(materia* materias){
	
	int aux1, aux2;
	for(aux1 = 0; aux1 < 4; aux1++){ // aux1 < nro amterias -1
		for(aux2 = aux1+1; aux2 < 5; aux2++){ // aux < nromaterias
			if(materias[aux1].turmaEscolhida != -1 && materias[aux2].turmaEscolhida != -1){	//SE JÁ FOI ESCOLHIDA ALGUMA TURMA PRA MATERIA (CHECANDO A FLAG)
				if(hasConflict(materias[aux1].turmas[materias[aux1].turmaEscolhida], materias[aux1].creditos/2, materias[aux2].turmas[materias[aux2].turmaEscolhida], materias[aux2].creditos)){
//NA LINHA ACIMA ELE CHECOU SE HÁ CONFLITO ENTRE AS TURMAS ESCOLHIDAS JÁ PREENCHIDAS, ISSO É, =! -1, ELE FAZ ESSA COMPARAÇÃO PARA TODAS AS MATERIAS
					return 1; //SE HÁ CONFLITO ELE RETORNA 1
				}
			}
		}
	}
	return 0; //SE NÃO ELE RETORNA 0
}

grade* montaUmaGrade(grade* grades, int* nroDeGrades,materia* materias){
	grades = realloc(grades, ((*nroDeGrades)+1)*sizeof(grade));	//realoca espaço pra mais 1 grade (não é o modo mais seguro mas por hora vai servir)
	//metodo seguro: cria uma variavel auxiliar e realoca nela, se a variavel aux != NULL (se a alocação ocorreu com sucesso) então grades = aux;
	grades[(*nroDeGrades)].turmasEscolhidas = malloc(5*sizeof(int));   //aloca espaço pra 5 materias dentro da grade.
	for(int i = 0; i < 5; i++){		//vai por todas as materias
		grades[(*nroDeGrades)].turmasEscolhidas[i] = materias[i].turmaEscolhida; //salva a turma escolhida de cada materia
	}
	(*nroDeGrades) = (*nroDeGrades) + 1; //+1 grade encontrada!
	return grades;	//retorna o endereço de memoria realocado
}

//método para percorrer todas as turmas de todas as disciplinas procurando grades válidas
grade* testaTodasTurmas(materia* materias, int nroMaterias, int materiaAtual, grade* grades, int* nroDeGrades){
	int i;
	for (i = 0; i < materias[materiaAtual].nroTurmas; i++){//percorre todas as turmas
		materias[materiaAtual].turmaEscolhida = i;//coloca a turma como a turma "escolhida" -> a turma escolhida é a turma que vai ser testada para checar se existe uma grade plausivel para ela
		if(materiaAtual +1 == nroMaterias){	//se for a ultima materia
			if(checkGradeConflict(materias) == 0){ //e se não tiver conflito
				grades = montaUmaGrade(grades, nroDeGrades, materias); //monta uma grade com todas as turma escolhidas atuais
			} //se tiver conflito procura outra turma
		}
		else{ //se não for a ultima matéria
			grades = testaTodasTurmas(materias, nroMaterias, materiaAtual+1, grades, nroDeGrades); //chama a proxima matéria
		}
	}
	return grades; //retorna o endereço das turmas
}

//montando grades por força bruta
//acho que esse método pode ser retirado a chamar a testaTodasTurmas direto da main, mas to sem cabeça pra resolver isso agora, mas deve ser simples.
grade* montaTodasAsGrades(int numeroDeMaterias, materia* materias, int* nroDeGrades){
	grade* grades = NULL;			//inicia o ponteiro como null
	grades = testaTodasTurmas(materias, numeroDeMaterias, 0, grades, nroDeGrades); //o ponteiro vai receber um endereço de testa todas as turmas
		//ps: a variavel nroDeGrades também será alterada, por isso estou passando-a por referencia
	return grades;	//retorna o endereço de memoria que se encotnra as grades montadas
}

//equivalente ao método getDia_STR() da classe horario
char* intToDay(int dia){
	char* szDia = malloc(10);
	switch(dia){
		case 0:
			strcpy(szDia, "segunda");
			break;
		case 1:
			strcpy(szDia, "terça");
			break;
		case 2:
			strcpy(szDia, "quarta");
			break;
		case 3:
			strcpy(szDia, "quinta");
			break;
		case 4:
			strcpy(szDia, "sexta");
			break;
		case 5:
			strcpy(szDia, "sabado");
			break;
		case 6:
			strcpy(szDia, "domingo");
	}
	return szDia;
}

int main(){
	materia* materias;
	
	//o codigo abaixo apenas inicia as matérias, no nosso programa esses dados serão lidos diretamente do mwscanner e serão as matérias escolhidas pelo usuário

	materias = malloc(5*sizeof(materia)); //cria 5 materias
	
	
	//criando materia 1:
	strcpy(materias[0].nome, "Calculo 1");
	materias[0].prioridade = 1; //prioridade mais alta
	materias[0].nroTurmas= 3;
	materias[0].creditos = 6;
	materias[0].turmaEscolhida = -1; //flag
	//criando as turmas da materia 1:
	materias[0].turmas = malloc(3*sizeof(turma));
	//AA:
	strcpy(materias[0].turmas[0].sigla, "AA");
	materias[0].turmas[0].horarios = malloc(3*sizeof(aula));
	materias[0].turmas[0].horarios[0].dia = 0; //segunda
	materias[0].turmas[0].horarios[0].horario = 8; //8h
	materias[0].turmas[0].horarios[1].dia = 2; //quarta
	materias[0].turmas[0].horarios[1].horario = 8; //8h
	materias[0].turmas[0].horarios[2].dia = 4; //sexta
	materias[0].turmas[0].horarios[2].horario = 8; //8h
	//BB:
	strcpy(materias[0].turmas[1].sigla, "BB");
	materias[0].turmas[1].horarios = malloc(3*sizeof(aula));
	materias[0].turmas[1].horarios[0].dia = 0; //segunda
	materias[0].turmas[1].horarios[0].horario = 14; //14h
	materias[0].turmas[1].horarios[1].dia = 1; //terça
	materias[0].turmas[1].horarios[1].horario = 14; //14h
	materias[0].turmas[1].horarios[2].dia = 3; //quinta
	materias[0].turmas[1].horarios[2].horario = 14; //14h
	//CC:
	strcpy(materias[0].turmas[2].sigla, "CC");
	materias[0].turmas[2].horarios = malloc(3*sizeof(aula));
	materias[0].turmas[2].horarios[0].dia = 0; //segunda
	materias[0].turmas[2].horarios[0].horario = 8; //8h
	materias[0].turmas[2].horarios[1].dia = 1; //terça
	materias[0].turmas[2].horarios[1].horario = 8; //8h
	materias[0].turmas[2].horarios[2].dia = 3; //quinta
	materias[0].turmas[2].horarios[2].horario = 8; //8h
	
	
	//criando materia 2:
	strcpy(materias[1].nome, "Desenho de Software");
	materias[1].prioridade = 2; //2ª prioridade mais alta
	materias[1].nroTurmas= 1;
	materias[1].creditos = 4;
	materias[1].turmaEscolhida = -1; //flag
	//criando as turmas da materia 2:
	materias[1].turmas = malloc(1*sizeof(turma));
	//AA:
	strcpy(materias[1].turmas[0].sigla, "A");
	materias[1].turmas[0].horarios = malloc(2*sizeof(aula));
	materias[1].turmas[0].horarios[0].dia = 0; //segunda
	materias[1].turmas[0].horarios[0].horario = 8; //8h
	materias[1].turmas[0].horarios[1].dia = 4; //sexta
	materias[1].turmas[0].horarios[1].horario = 8; //8h
	
	
	//criando materia 3:
	strcpy(materias[2].nome, "Estrut de Dados e Algoritmos ");
	materias[2].prioridade = 4; //4ª prioridade mais alta
	materias[2].nroTurmas= 3;
	materias[2].creditos = 4;
	materias[2].turmaEscolhida = -1; //flag
	//criando as turmas da materia 3:
	materias[2].turmas = malloc(3*sizeof(turma));
	//A:
	strcpy(materias[2].turmas[0].sigla, "A");
	materias[2].turmas[0].horarios = malloc(2*sizeof(aula));
	materias[2].turmas[0].horarios[0].dia = 1; //terça
	materias[2].turmas[0].horarios[0].horario = 8; //8h
	materias[2].turmas[0].horarios[1].dia = 3; //quinta
	materias[2].turmas[0].horarios[1].horario = 8; //8h
	//B:
	strcpy(materias[2].turmas[1].sigla, "B");
	materias[2].turmas[1].horarios = malloc(2*sizeof(aula));
	materias[2].turmas[1].horarios[0].dia = 1; //terça
	materias[2].turmas[1].horarios[0].horario = 16; //16h
	materias[2].turmas[1].horarios[1].dia = 3; //quinta
	materias[2].turmas[1].horarios[1].horario = 16; //16h
	//C:
	strcpy(materias[2].turmas[2].sigla, "C");
	materias[2].turmas[2].horarios = malloc(2*sizeof(aula));
	materias[2].turmas[2].horarios[0].dia = 2; //quarta
	materias[2].turmas[2].horarios[0].horario = 8; //10h
	materias[2].turmas[2].horarios[1].dia = 4; //sexta
	materias[2].turmas[2].horarios[1].horario = 8; //10h

	//criando materia 4:
	strcpy(materias[3].nome, "ORIENTACAO A OBJ");
	materias[3].prioridade = 3; //3ª prioridade mais alta
	materias[3].nroTurmas= 2;
	materias[3].creditos = 4;
	materias[3].turmaEscolhida = -1; //flag
	//criando as turmas da materia 4:
	materias[3].turmas = malloc(2*sizeof(turma));
	//A:
	strcpy(materias[3].turmas[0].sigla, "A");
	materias[3].turmas[0].horarios = malloc(2*sizeof(aula));
	materias[3].turmas[0].horarios[0].dia = 2; //quarta
	materias[3].turmas[0].horarios[0].horario = 14; //14h
	materias[3].turmas[0].horarios[1].dia = 4; //sexta
	materias[3].turmas[0].horarios[1].horario = 14; //14h
	//B:
	strcpy(materias[3].turmas[1].sigla, "B");
	materias[3].turmas[1].horarios = malloc(2*sizeof(aula));
	materias[3].turmas[1].horarios[0].dia = 1; //terça
	materias[3].turmas[1].horarios[0].horario = 10; //10h
	materias[3].turmas[1].horarios[1].dia = 3; //quinta
	materias[3].turmas[1].horarios[1].horario = 10; //10h


	//criando materia 5:
	strcpy(materias[4].nome, "MET NUM P ENG");
	materias[4].prioridade = 3; //3ª prioridade mais alta
	materias[4].nroTurmas= 4;
	materias[4].creditos = 4;
	materias[4].turmaEscolhida = -1; //flag
	//criando as turmas da materia 3:
	materias[4].turmas = malloc(4*sizeof(turma));
	//A:
	strcpy(materias[4].turmas[0].sigla, "A");
	materias[4].turmas[0].horarios = malloc(2*sizeof(aula));
	materias[4].turmas[0].horarios[0].dia = 1; //ter
	materias[4].turmas[0].horarios[0].horario = 14; //14h
	materias[4].turmas[0].horarios[1].dia = 3; //qui
	materias[4].turmas[0].horarios[1].horario = 14; //14h
	//B:
	strcpy(materias[4].turmas[1].sigla, "B");
	materias[4].turmas[1].horarios = malloc(2*sizeof(aula));
	materias[4].turmas[1].horarios[0].dia = 1; //terça
	materias[4].turmas[1].horarios[0].horario = 14; //14h
	materias[4].turmas[1].horarios[1].dia = 3; //quinta
	materias[4].turmas[1].horarios[1].horario = 14; //14h
	//C:
	strcpy(materias[4].turmas[2].sigla, "C");
	materias[4].turmas[2].horarios = malloc(2*sizeof(aula));
	materias[4].turmas[2].horarios[0].dia = 0; //seg
	materias[4].turmas[2].horarios[0].horario = 16; //16h
	materias[4].turmas[2].horarios[1].dia = 2; //qua
	materias[4].turmas[2].horarios[1].horario = 16; //16h
	//D:
	strcpy(materias[4].turmas[3].sigla, "D");
	materias[4].turmas[3].horarios = malloc(2*sizeof(aula));
	materias[4].turmas[3].horarios[0].dia = 0; //seg
	materias[4].turmas[3].horarios[0].horario = 16; //16h
	materias[4].turmas[3].horarios[1].dia = 2; //qua
	materias[4].turmas[3].horarios[1].horario = 16; //16h
	char* ids[10];
	int aux;
	for(aux = 0; aux < 10; aux++){
		ids[aux] = malloc(11); // aloca a memoria
		ids[aux][0] = '\0'; //inicia as strings como \zero char
	}
	aux = 0;
	organizaMatPrioridade(materias, 5); //organiza por prioridade
	int nroDeGRadesMontadas;	//para controlar o numero de grades (acho que no java tem algum método de size para vetores (e.g: grades.size), mas em c n tem
	//chama a função de montar grades
	grade* grades = montaTodasAsGrades(5, materias, &nroDeGRadesMontadas);

	//o codigo abaixo apenas imprime a grade
	printf("Foram montadas um total de %d grades:\n", nroDeGRadesMontadas);
	for(int i = 0; i < nroDeGRadesMontadas; i++){
		printf("Grade %d:\n", i+1);
		for(int j = 0; j < 5; j++){
			printf("\tMateria: %s\n\tTurma:%s\n", materias[j].nome, materias[j].turmas[grades[i].turmasEscolhidas[j]].sigla);
			printf("\tHorarios:\n");
			int aulas = materias[j].creditos/2;
			for(int k = 0; k < aulas; k++){
				printf("\t\t%s - %dh\n", intToDay(materias[j].turmas[grades[i].turmasEscolhidas[j]].horarios[k].dia), materias[j].turmas[grades[i].turmasEscolhidas[j]].horarios[k].horario);
			}
		}
	}

	return 0;
}


