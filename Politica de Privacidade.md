#Politica de Privacidade
-----------------
A Equipe Atlas sabe da importância que tem a privacidade para o usuário. Essa política de privacidade trata de quais dados coletamos do usuário, e de como usamos esses dados, e também ao que o usuário se expõe.
Ao usar o nosso serviço (UniGrade), o usuário adiciona informações importantes sobre sua rotina e seu horário semanal. Porém, todos os dados que o usuário insere no aplicativo ao selecionar turmas, e matérias desejadas, são armazenadas no próprio aparelho do usuário, não tendo qualquer ligação direta e indireta com o nosso banco de dados.
O aplicativo também, não necessita de login do usuário, fato que deixa mais simples e viável o uso anônimo do aplicativo. 
	Ao montar a grade desejada, o aplicativo intuitivamente cria uma pasta em sua galeria e baixa (por escolha o usuário), uma imagem do tipo (.jpg), na galeria. Tomando espaço da memória do aparelho.
	O aplicativo atualmente está restrito ao uso de estudantes da Universidade de Brasília, sendo assim nenhuma forma de rastreio é utilizado, pois por enquanto nosso público-alvo ainda se encontra restrito.
	
