package com.atlas.unigrade;

import android.content.Context;
import android.util.Log;

import java.io.File;


/***
 * Essa classe é responsável por guardar informações sobre os locais onde temos dados no android
 * */
public class BaseDeDados {

    private static File raizBaseDeDados;
    private static File raizArquivosElencados;
    private static String materiaParaLer;

    private static File raizDiretorioArquivos;

    static String getMateriaParaLer (){

        return materiaParaLer;
    }
	//metodo set que recebe nome da materia como parametro para aloca-la à outra variavel.
    static void setMateriaParaLer(String nomeDaMateria){

        materiaParaLer = nomeDaMateria;
    }

	//metodos sets que retornam valores para os objetos.
    static File getRaizBaseDeDados(){

        if(raizBaseDeDados == null){
            recuperarBaseDeDados(raizDiretorioArquivos);
        }

        return raizBaseDeDados;
    }

    static File getRaizArquivosElencados(){

        if(raizArquivosElencados == null){
            recuperarBaseDeDados(raizDiretorioArquivos);
        }

        return raizArquivosElencados;
    }

    /***
     * Este método serve para inicializar as variaves que são necessárias para a execução do código.
     * Ele procura as pastas utilizadas pelo aplicativo para armazenar os dados e os deixa expostos
     * nos membros estáticos desta classe
     * */
    static void inicializarBasesDeDados(File raizDiretorioArquivos, Context c){

        //salvar para uso futuro
        BaseDeDados.raizDiretorioArquivos = raizDiretorioArquivos;

        recuperarBaseDeDados(raizDiretorioArquivos);


        //cria estrutura base dentro da pasta
        if(raizArquivosElencados.exists()){
            //cria a estrutura base dos arquivos
            File codMat = new File(BaseDeDados.raizArquivosElencados.getAbsolutePath() + File.separator + "codMat.txt");
            File matCod = new File(BaseDeDados.raizArquivosElencados.getAbsolutePath() + File.separator + "matCod.txt");

            //escreve dados nos arquivos
            Escritor esc = new Escritor(c, codMat);
            esc.escreverLinha("");

            //se não criarmos um novo objeto, ele não será capaz de escrever
            esc = new Escritor(c, matCod);
            esc.escreverLinha("");
        }
    }

    /***
     * Este metodo se assemelha muito ao inicializarBaseDeDados, porem ele não pede um context para
     * ser executado. O outro metodo precisa dele para criar um Escritor, o que não é necessário aqui,
     * pois confiamos que o arquivo não precise de uma escrita inicial.
     * */
    private static void recuperarBaseDeDados(File raizDiretorioArquivos){

        BaseDeDados.raizBaseDeDados = new File(raizDiretorioArquivos.getAbsolutePath() + File.separator + "db");

        if(!raizBaseDeDados.exists()){
            raizBaseDeDados.mkdir();
        }else{
            File[] arquivos = raizBaseDeDados.listFiles();

            if (arquivos != null && arquivos.length == 1){
                BaseDeDados.raizBaseDeDados = new File(raizBaseDeDados.getAbsolutePath() + File.separator + arquivos[0].getName());
            }
        }

        BaseDeDados.raizArquivosElencados = new File(raizDiretorioArquivos.getAbsolutePath() + File.separator + "elencados");

        //se a pasta não existir, criar
        if(!raizArquivosElencados.exists()) {

            //cria a pasta de arq enlencados
            boolean b = BaseDeDados.raizArquivosElencados.mkdir();

            if (b == false) {
                Log.e("LEITOR", "Não foi possível criar o diretório de arquivos elencados em: " + BaseDeDados.raizArquivosElencados);
            }
        }



    }


	//Metodo que apaga todos os dados da base de dados, fazendo a checagem do metodo "deletar" e do metodo "imprimeSucesso" 
    static void apagarTodosOsDados(){


        boolean sucesso;
        sucesso =  deletar(raizBaseDeDados);

        imprimeSucesso(sucesso, "Apagar raizBaseDeDados ("+ raizBaseDeDados.getAbsolutePath() +")");

        sucesso = deletar(raizArquivosElencados);

        imprimeSucesso(sucesso, "Apagar raizArquivosElencados ("+ raizArquivosElencados.getAbsolutePath() +")");
    }
//Usado no metodo anterior
    private static void imprimeSucesso(boolean sucesso, String operacao){

        Log.d("BASEDEDADOS", operacao + (sucesso ? "SUCESSO" : "FALHOU"));

    }

    static boolean deletar(File file){

        //se for um diretório, precisamos esvaziar ele primeiro
        if(file.isDirectory()){
            //verifica se está vazio
            File conteudo[] = file.listFiles();

            if(conteudo != null || conteudo.length > 0){

                //se houver algum arquivo, apagar ele
                for(File f : conteudo){
                    //e se for uma pasta, vamos ter recursão nela
                    deletar(f);
                }
            }

        }

        return file.delete();
    }

    static void checarIntegridadeDeTexto(Context c){

//metodo try/catch para caso esse erro se concretize, o programa continue rodando, por ter essa excessão
        try {
            // returns pathnames for files and directory
            File[] materias = raizBaseDeDados.listFiles();

            // for each pathname in pathname array
            for(File caminho: materias) {
                File[] turmas = caminho.listFiles();
                for(File caminho_turmas: turmas){
                    Leitor leitor = new Leitor(caminho_turmas, c);
                    leitor.lerArquivo();
                    for (String s : leitor.getLido()) {
                        if (s.contains(">") || s.contains("<")){
                            Log.e("BASEDEDADOS", "Erro em: " + caminho_turmas.getAbsolutePath());
                        }

                    }

                }
            }

        } catch(Exception e) {

            // Se ocorrer algum erro
            e.printStackTrace();
        }

    }


}
