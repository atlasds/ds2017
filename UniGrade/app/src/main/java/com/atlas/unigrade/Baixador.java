package com.atlas.unigrade;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InterfaceAddress;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.FileHandler;

import javax.net.ssl.HttpsURLConnection;


/***
 * O código dessa classe foi feito baseado em tutoriais online. Comentei ao máximo para aumentar o
 * conhecimento sobre o código.
 * */
public class Baixador extends AsyncTask<String, String, String>  {

    //o que será chamado quando acabar o download
    DownloadCallback callback;

    private Context context;

    private String downloadLink;

    private FileDescriptor fd;
    private File file;
    private String localDoDownload;


    /***
     * metodo construtor, requer uma string que aponta para o local do download, o Context da aplica
     * ção androis e um callback para chamar quando acabar o download.
     */
    public Baixador(String downloadLocation, Context context, Atualizador objCallback, String link){
        this.context = context;
        this.callback = objCallback;
        this.localDoDownload = downloadLocation;
        this.downloadLink = link;

        Log.i("BAIXADOR", "Criando baixador com parametros:" +
                "\nLocal do download: " + downloadLocation +
                "\nLink: " + link);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute(); //questão de herança, aqui é tudo resolvido na classe mãe (super class)
    }

    @Override
    //esse processo roda de forma Async (no plano de fundo)
    protected String doInBackground(String... aurl) {
        //sobre os parametros desse metodo, vale a pena ler: https://stackoverflow.com/questions/3158730/java-3-dots-in-parameters
        //isso quer dizer que podemos passar uma quantidade arbitraria de strings para essa função e
        //ela deve continuar funcionando numa boa
        int count;

        try {

            //cria um objeto URL, isso faz parte do networking do java
            URL url = new URL(this.downloadLink);
            //abre a conexão com determinada URL
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

            connection.setConnectTimeout(3000);
            connection.setInstanceFollowRedirects(true);
            connection.connect(); //e finalmente, conecta

            //abre um buffer parar receber o arquivo
            InputStream input = new BufferedInputStream(url.openStream());

            //como estamos baixando um arquivo, a conexão tem um "tamanho"
            //então nós pegamos esse tamanho e salvamos
            int tamanhoArquivo = connection.getContentLength();

            //a aponta o membro file deste objeto para uma nova File no local do download
            file = new File(localDoDownload);

            //e aqui é onde vamos jogar o buffer de input aberto lá atras
            FileOutputStream output = new FileOutputStream(file); //context.openFileOutput("content.zip", Context.MODE_PRIVATE);

            fd = output.getFD();

            //cria 1MB
            byte data[] = new byte[4 * 1024];
            long total = 0; //essa variavel salva quantos bytes usamos

            //count recebe input.read(data), enquanto isso for diferente de -1 faça
            //será -1 quando o input chegar ao fim
            count = input.read(data);

            while (count != -1){

                total += count;

                ///envia o progresso da tarefa aysnc (calculado com base no tamanho e no progresso)
                publishProgress(""+(int)((total*100)/tamanhoArquivo));
                //escreve o que foi baixado
                output.write(data, 0, count);

                count = input.read(data);
            }

            //fecha tudo
            output.flush();
            output.close();
            input.close();


        } catch (Exception e) {

            e.printStackTrace();
            this.callback.notificarErro(e);
        }

        return null;
    }

    /***
     * Metodo chamado pelo sistema após receber um 'publishProgress' da tarefa Async.
     * */
    protected void onProgressUpdate(String... progress) {

        Integer progressoInt[] = new Integer[progress.length];
        int i = 0;

        for(String s : progress){
            progressoInt[i] = Integer.parseInt(s);
            i++;
        }

        Log.i("BAIXADOR", "Progresso do download de: " + downloadLink
                +"\n" + progressoInt[0] + "%");

        callback.definirProgresso(progressoInt); //envia para o objeto callback as informações sobre
                                             //o progresso atual
    }

    @Override
    /***
     * Metodo chamado ao final da tarefa Async
     * */
    protected void onPostExecute(String unused) {
        if(callback != null){

            if(file != null && file.exists()){

                Log.i("BAIXADOR", "Downlaod completo, salvo em: " + file.getAbsolutePath());

                callback.downloadCompleto(file);
            }else{

                Log.i("BAIXADOR", "Erro no download: " + unused);

                callback.notificarErro(new Exception("Download nao completo"));
            }


        }
    }

}
