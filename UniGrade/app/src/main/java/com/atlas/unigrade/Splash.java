package com.atlas.unigrade;

/**
 * Created by geovana on 22/09/17.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class Splash extends Activity {
    // Timer da splash screen
    private static int DURACAO_SPLASH = 3000;

    public static boolean atualizacaoEncontrada = false;
    public static boolean estaAtualizando = false;

    public static boolean tempoAcabou = false;
    public static boolean instalacaoIniciada = false;
    public static boolean instalacaoCompleta = false;

    Atualizador att;

    TextView statusDownload;
    int progressoDownload; //esta int representa o progresso do download

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        atualizacaoEncontrada = false;
        tempoAcabou = false;
        instalacaoIniciada = false;
        instalacaoCompleta = false;


        setContentView(R.layout.activity_splash);

        //todo: escrever: procurando atualização

        //para procurar atualizações precisamos da base de dados
        BaseDeDados.inicializarBasesDeDados(getFilesDir(), getApplicationContext());

        if(!estaAtualizando ) {

            //cria o handler para daqui a tantos segundos, se achar a atualização até lá, perguntar se
            //deseja baixarAtualizacao, senão, seguir normalmente.
            new Handler().postDelayed(new Runnable() {
                /*
                 * Exibindo splash com um timer.
                 */
                @Override
                public void run() {

                  iniciarApp();
                }
            }, DURACAO_SPLASH);

            //a verificação de atualização ocorre em outro thread para não bloquear o thread de UI
            Runnable r = new Runnable() {
                @Override
                public void run() {

                    Atualizador at = new Atualizador(getApplicationContext());
                    at.procurarAtualizacao();

                }
            };

            r.run();

        }else{

            estaAtualizando = false;
            atualizar();
        }
    }

    void atualizar(){
        atualizacaoEncontrada = false;

        //"reiniciamos" o app no modo atualizador
        att = new Atualizador(getApplicationContext());
        att.baixarAtualizacao();

        this.progressoDownload = -1;
        this.statusDownload = (TextView) findViewById(R.id.statusAtualizacao);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                duracaoSplash();
            }
        }, 10000);

        atualizarStatusDownload();
    }

    void atualizarStatusDownload(){

        progressoDownload = att.statusDownload();

        if(progressoDownload >= 0) {
            statusDownload.setText("Baixando... " + progressoDownload + "%");
        }else{
            statusDownload.setText("Conectando... ");
        }

        if(!att.acabouDownload()) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    atualizarStatusDownload();
                }
            }, 500);

        }else{
           instalarAtualizacao();
        }
    }

    private void duracaoSplash(){
        if(att.temErro() || att.statusDownload() <= 0){

            this.tempoAcabou = true;

            att.setTempoAcabou(true);
        }
    }

    private void instalarAtualizacao(){

        if(att.temErro()){
            //TODO: lidar com o erro
            iniciarApp();
        }else{

            statusDownload.setText("Instalando atualização...");

            instalacaoIniciada = true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    rodarInstalacao();
                }
            }, 500);
        }
    }

    private void rodarInstalacao(){

        boolean sucesso = att.instalarAtualizacao();

        if(sucesso){
            instalacaoCompleta = true;
        }

        iniciarApp();
    }

    void iniciarApp(){

        // Esse método será executado sempre que o timer acabar
        // E inicia a activity principal
        Intent i = new Intent(getApplicationContext(), telaPrincipal.class);
        startActivity(i);

        // Fecha esta activity
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 3000);
    }
}
