package com.atlas.unigrade;

import android.content.Context;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import static com.atlas.unigrade.OneFragment.limparString;

public class Pesquisador implements Runnable{

    private LinkedList<String> baseDeProcura;
    private String oQuePesquisar;

    private Timer tempoDeEspera;

    //tempo de espera, em ms
    private static final int TEMPO_DE_ESPERA= 360;

    private static boolean pesquisaAguardando = false; //indica se um pedido de pesquisa foi feito
    private boolean executar = true; //caso esse atributo seja falso, a pesquisa para

    private static boolean estaPesquisando = false; //indica se está ocorrendo uma pesquisa
    private static final ThreadLocal<Pesquisador> pesquisadorAtual = new ThreadLocal<>(); //representa o objeto pesquisador que está sendo executado

    private IPesquisadorCallback callback;

    private Context context; //precisamos do context para poder puxar uma string de R

    private Pesquisador(LinkedList<String> baseDeProcura, String oQuePesquisar, IPesquisadorCallback callback, Context c){

        this.baseDeProcura = baseDeProcura;
        this.oQuePesquisar = oQuePesquisar;
        this.callback = callback;
        this.context = c;

    }

    //esse metodo é chamado para configurar a busca e executa-la
    public static void criarPesquisa(LinkedList<String> baseDeProcura, String oQuePesquisar, IPesquisadorCallback callback, Context c){

        //se tivermos uma pesquisa já preparada, então devemos cancelar ela
        if(pesquisaAguardando){

            pesquisadorAtual.get().pararTimer();
        }


        //se uma pesquisa estiver sendo executada, paramos ela
        if(estaPesquisando){
            //parar pesquisa para criar uma nova

            pesquisadorAtual.get().stop();
        }

        //então criamos um novo pesquisador
        pesquisadorAtual.set(new Pesquisador(baseDeProcura, oQuePesquisar, callback, c));

        //e dizemos que temos um pesquisa preparada
        pesquisaAguardando = true;

        //entretanto, queremos esperar um pouco para rodar o pesquisador
        //a ideia é rodar ele somente quando o usuário parar de digitar

        //entao vamos criar um timer que espera algum tempo antes de disparar a pesquisa
        pesquisadorAtual.get().iniciarTimer();

        //caso esta função seja chamada novamente, então esse timer vai parar de ser executado e o
        //objeto pesquisador não será executado
    }

    private void iniciarTimer(){

        if(this.tempoDeEspera == null){
            tempoDeEspera = new Timer();
        }

        //criamos o timer para começar a pesquisa em 500 ms
        this.tempoDeEspera.schedule(new TimerTask() {
            @Override
            public void run() {
                executarPesquisa();
            }
        }, TEMPO_DE_ESPERA);

    }


    private void pararTimer() {

        pesquisaAguardando = false;

        this.tempoDeEspera.cancel();
        this.tempoDeEspera.purge();
    }

    private void executarPesquisa(){

        run();

    }


    @Override
    public void run() {

        pesquisaAguardando = false;
        estaPesquisando = true;

        //criamos uma cópia da lista
        LinkedList<String> materias = new LinkedList<>();

        //agora vamos filtrar o que precisamos
        //por enquanto vamos pesquisar de forma burra
        for (String daLista : baseDeProcura){

            if(executar == false){

                pesquisadorAtual.remove();

                return; //paramos a execução caso seja pedido
            }

            //para comparar também limpamos o que está na lista
            String limpa = limparString(daLista.toLowerCase());

            if(limpa.contains(oQuePesquisar)){

              //  Log.i("PESQUISA", "Encontrada: " + daLista);
                materias.add(daLista);

            }
        }

        estaPesquisando = false;

        if(materias.size() < 1){
            materias.add(context.getString(R.string.semResultadosNaPesquisa).toString());
        }

        final LinkedList<String> materiasFinal = materias;

        callback.postarResultadoPesquisa(materiasFinal);


        pesquisadorAtual.remove();

    }

    private void stop(){
        executar = false;
        estaPesquisando = false;
    }

}
