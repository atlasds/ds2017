package com.atlas.unigrade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class JanelaSobre extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_janela_sobre);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarsobre);
        toolbar.setTitle("Sobre nós");
        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher_tpp);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        TextView versao = (TextView) findViewById(R.id.textoVersão);
        versao.setText("Versão atual: " + BuildConfig.VERSION_NAME + " / id:" + BuildConfig.VERSION_CODE);


    }
}
