package com.atlas.unigrade;


import android.support.annotation.NonNull;

/***
A classe turma tem o registro de horários de uma turma e a letra que representa a turma além de seu
 professor.
*/

public class Turma implements Comparable<Turma>{
	
	private String identificadorDaTurma;
	private String professor;
	private String campus;
	
	private int qtdAulas;
	private Horario[] aulas;

	boolean checkbox;
	
	//Inicializador
	/***
	 *  É necessário montar uma array com os horarios da turma e passar como argumento
	 *	param: String identificadorDaTurma, Horario[] aulas
	 **/
	Turma(String identificadorDaTurma, String professor, Horario[] aulas, String campus){
		
		this.identificadorDaTurma = identificadorDaTurma.replace(" ", "");
		this.professor = professor;

		if(aulas != null) {
		//Condicional que define quantidade de aulas para turma / materia
			qtdAulas = aulas.length;
			this.aulas = new Horario[qtdAulas];

			this.aulas = aulas;
		}else{

			this.qtdAulas = 0;
			this.aulas = null;
		}

        this.campus = campus;

	}
	
	String getIdentificadorDaTurma() { //Getter para identificador da turma
		return this.identificadorDaTurma;
	}
	
	String getProfessor() { //Getter para o professor da turma
		return this.professor;
	}

    public String getCampus() { //Getter de campus
        return campus;
    }


    /***
	 * Determina se esta turma possui aula em um horario h
	 * */
	boolean temAula(Horario h) {

		if(this.aulas == null){
			return false;
		}
		
		for (Horario aula : this.aulas) {
			if(aula.temChoque(h))  {
				return true;
			}
		}
		
		return false;
		
	}
	
	Horario[] getAulas() {
		return this.aulas;
	}
	
	/**
	 * Esse metodo nao depende de um objeto para ser chamado,
	 * ele indica se existe um conflito de horarios entre duas turmas
	 * */
	public static boolean temConflito(Turma a, Turma b) {
		
		//para cada aula em aulas[] da turma A
		for (Horario aula : a.getAulas()) {
			
			//verificar de a turma B possui aula no mesmo horario
			if(b.temAula(aula)) {
				return true;
			}
		}
		
		return false;
	}

	//CheckBox
	public boolean isCheckbox() {
		return checkbox;
	}

	public void setCheckbox(boolean checkbox) {
		this.checkbox = checkbox;
	}


	@Override
	public int compareTo(@NonNull Turma o) {

		return this.getIdentificadorDaTurma().compareTo(o.getIdentificadorDaTurma());
	}
}
