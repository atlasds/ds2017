package com.atlas.unigrade;

import android.content.Context;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by Guilherme on 22/09/2017.
 */

public class Escritor {

    private File caminhoArquivo = null;

    private Context context; //Variável importada da biblioteca Android em Java.

    private BufferedWriter buffer;


    Escritor(Context c, File caminhoArquivo){

        this.caminhoArquivo = caminhoArquivo; //Alocando a referencia da variável File, para a classe Escritor
        this.context = c;
    }

    Escritor(Context c){

        this.context = c;
    }

    private void criarBuffer() throws FileNotFoundException, UnsupportedEncodingException {

        //sem argumentos, o metodo cria um buffer no modo APPEND

        if(this.caminhoArquivo == null){
            throw new FileNotFoundException();
        }

        if(this.caminhoArquivo.exists()){
            this.buffer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(this.caminhoArquivo, true))
            );
        }else{
            //se o arquivo estiver sendo criado, não há como dar append
            this.buffer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(this.caminhoArquivo, false))
            );
        }
    }


    private void criarBuffer(boolean append) throws FileNotFoundException, UnsupportedEncodingException {

        if(this.caminhoArquivo == null){
            throw new FileNotFoundException();
        }

        this.buffer = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(this.caminhoArquivo, append))
        );
    }

    /***
     * Adicona linha ao final do arquivo
     * */
    protected boolean escreverLinha(String linha){

        try{

            if(this.buffer == null){
                criarBuffer();
            }

            this.buffer.write(linha.toCharArray());
            this.buffer.newLine();

            this.buffer.flush();
            this.buffer.close();

            return  true;

        }catch (Exception e){
            e.printStackTrace();

            return  false;
        }

    }

    /***
     * Adicona linhas ao final do arquivo
     * */
    private boolean escreverLinhas(LinkedList<String> linhas){
        try{

            if(this.buffer == null){
                criarBuffer();
            }

            if(linhas == null || linhas.size() == 0){
                return false;
            }

            for (String linha : linhas) {

                this.buffer.write(linha.toCharArray());
                this.buffer.newLine();
            }


            this.buffer.flush();
            this.buffer.close();

            return  true;

        }catch (Exception e){
            e.printStackTrace();

            return  false;
        }
    }

    /***
     * Escreve por cima do conteudo do arquivo existente
     * */
    private boolean sobrescreverLinhas(LinkedList<String> linhas){
        try {

            if(this.caminhoArquivo.exists()){
                //caso o arquivo exista ele deve ser apagado, pois será escrito novamente
                this.caminhoArquivo.delete();
            }

            if (this.buffer == null) {
                //a unica diferença é que o buffer deve ser criado sem usar o modo APPEND
                criarBuffer(false);
            }

            return escreverLinhas(linhas);
        }catch (Exception e){

            e.printStackTrace();

            return false;
        }
    }

    private boolean sobrescreverLinha(String linha){
        try {

            if(this.caminhoArquivo.exists()){
                //caso o arquivo exista ele deve ser apagado, pois será escrito novamente
                this.caminhoArquivo.delete();
            }

            if (this.buffer == null) {
                //a unica diferença é que o buffer deve ser criado sem usar o modo APPEND
                criarBuffer(false);
            }

            return escreverLinha(linha);
        }catch (Exception e){

            e.printStackTrace();

            return false;
        }
    }

    /***
     * Escreve o conteúdo da materia m no local indicado pela raizDoLocal, cuidado, esta função
     * sobrescreve os dados já presentes no local
     * raizDoLocal == raizBaseDados ou raizArquivosElencados
     * */
    boolean escreverMateria(Materia m, File raizDoLocal){

        File pastaDaMateria = new File(raizDoLocal + File.separator + m.getCodMat());
        File codMat = new File(raizDoLocal.getAbsolutePath() + File.separator + "codMat.txt");
        File matCod = new File(raizDoLocal.getAbsolutePath() + File.separator + "matCod.txt");

        if(m.getTurmas().size() < 1 && raizDoLocal.compareTo(BaseDeDados.getRaizArquivosElencados()) == 0){
            //como não há turmas, a matéria será apagada, caso raizDoLocal seja a raiz de arquivos
            //elencados

            removerDaLista(m.getCodMat(), codMat);
            removerDaLista(m.getMatCod(), matCod);

            return BaseDeDados.deletar(pastaDaMateria);

        }else {

            if (!pastaDaMateria.exists()) {
                pastaDaMateria.mkdir();
            } else {
                //vamos apagar os conteúdos de dentro da pasta para substituir com a materia que foi enviada
                BaseDeDados.deletar(pastaDaMateria);

                //recriar a pasta (vazia)
                pastaDaMateria.mkdir();
            }

            LinkedList<String> linhasAEscrever = new LinkedList<>(); //Lista de strings que forma todos os elementos do arquivo escrito.

            linhasAEscrever.add(m.getCodigo());
            linhasAEscrever.add(m.getNome());
            linhasAEscrever.add(m.getCreditos());
            linhasAEscrever.add(Integer.toString(m.getAulasSemanais()));
            linhasAEscrever.add(Integer.toString(m.getPrioridade()));

            File infoMateria = new File(pastaDaMateria.getAbsolutePath() + File.separator + "info.txt");

            Escritor esc = new Escritor(this.context, infoMateria);
            esc.escreverLinhas(linhasAEscrever);

            //Metodo que escreve turmas
            for (Turma t : m.getTurmas()) {
                esc = new Escritor(this.context, new File(pastaDaMateria + File.separator + t.getIdentificadorDaTurma() + ".txt"));

                linhasAEscrever.clear();

                if(t.getAulas() != null) {

                    for (Horario h : t.getAulas()) {
                        linhasAEscrever.add(h.getDia_STR());
                        linhasAEscrever.add(h.getInicio_STR());
                        linhasAEscrever.add(h.getFim_STR());
                    }
                }

                linhasAEscrever.add(t.getProfessor());

                linhasAEscrever.add(t.getCampus());

                esc.sobrescreverLinhas(linhasAEscrever);
            }

            this.adicionarALista(m.getCodMat(), codMat);
            this.adicionarALista(m.getMatCod(), matCod);

        }

        return true;
    }

    /***
     * Este metodo adiciona uma linha a um arquivo codMat ou matCod
     * */
    private boolean adicionarALista(String linha, File arquivo){

        if(!arquivo.getName().toLowerCase().contains("codmat") && !arquivo.getName().toLowerCase().contains("matcod")){
            //arquivo não é do tipo codmat ou matcod
            return false;
        }


        Escritor esc = new Escritor(this.context, arquivo);
        Leitor leitor = new Leitor(arquivo, this.context);

        //le o arquivo ignorando linhas vazias
        leitor.lerArquivo(true);

        if(leitor.getLido() == null || leitor.getLido().length == 0){
            //nada no arquivo
            return esc.sobrescreverLinha(linha);

        }else {
            //algo no arquivo

            String[] codParaAdicionar = Materia.interpretaCodMat(linha);
            String[] existente;

            boolean encontrada = false;

            for(String linhaLida : leitor.getLido()){
                existente = Materia.interpretaCodMat(linhaLida);


                //TODO: otimizar
                if(existente[0].toLowerCase().compareTo(codParaAdicionar[0].toLowerCase()) == 0){
                    encontrada = true;

                    break;
                }

                //só escrevemos se a matéria não estiver presente
                if(!encontrada){
                    //TODO: otimizar

                    //adiciona to-do o conteudo a uma lista
                    LinkedList<String> novoConteudoArquivo = new LinkedList<>();

                    Collections.addAll(novoConteudoArquivo, leitor.getLido());

                    //adiciona a linha que queremos
                    novoConteudoArquivo.add(linha);

                    //organiza a lista em 'ordem natural'
                    Collections.sort(novoConteudoArquivo);

                    //procura por objetos repetidos
                    for (int i = 0; i < novoConteudoArquivo.size() - 1; i++){
                        for(int j = i + 1; j < novoConteudoArquivo.size(); j++){
                            if(
                                Materia.interpretaCodMat(novoConteudoArquivo.get(i))[0]
                                    .compareTo(Materia.interpretaCodMat(novoConteudoArquivo.get(j))[0]) == 0
                            ){

                                        novoConteudoArquivo.remove(j);
                            }
                        }
                    }

                    //podemos chamar sobrescrever aqui, pois o buffer de 'esc' ainda não foi definido
                   return esc.sobrescreverLinhas(novoConteudoArquivo);
                }
            }

        }

        return false;
    }

    private boolean removerDaLista(String linha, File arquivo){
        if(!arquivo.getName().toLowerCase().contains("codmat") && !arquivo.getName().toLowerCase().contains("matcod")){
            //arquivo não é do tipo codmat ou matcod
            return false;
        }



        Leitor leitor = new Leitor(arquivo, this.context);

        //le o arquivo ignorando linhas vazias
        leitor.lerArquivo(true);

        if (leitor.getLido() != null && leitor.getLido().length > 0) {
            //algo no arquivo

            String[] codParaRemover = Materia.interpretaCodMat(linha);
            String[] existente;

            //isso deve remover todas as instancias de um determinado código dentro do arquivo

            //adiciona to-do o conteudo a uma lista
            LinkedList<String> novoConteudoArquivo = new LinkedList<>();
            Collections.addAll(novoConteudoArquivo, leitor.getLido());

            //para cada linha do arquivo
            for(String linhaLida : leitor.getLido()){
                existente = Materia.interpretaCodMat(linhaLida);

                //TODO: otimizar
                //quando a linha for referente a mesma matéria
                if(existente[0].toLowerCase().compareTo(codParaRemover[0].toLowerCase()) == 0){


                    //remove a linha que queremos
                    novoConteudoArquivo.remove(linhaLida);

                    //organiza a lista em 'ordem natural'
                    Collections.sort(novoConteudoArquivo);

                }
            }

            //vamos criar um novo escritor aqui para poder evitar problemas de buffer
            Escritor esc = new Escritor(this.context, arquivo);
            esc.sobrescreverLinhas(novoConteudoArquivo);

        }

        return false;
    }
}
