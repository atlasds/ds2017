package com.atlas.unigrade;

import android.support.annotation.Nullable;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class MontadorDeGrade implements Runnable {

    private LinkedList<Grade> gradesMontadas;

    private LinkedList<Materia> materiasSelecionadas; //utilizado para armazenar as materias escolhidas pelo usuario

    private IMontadorDeGradeCallback callback;

    private int maior;


    MontadorDeGrade(LinkedList<Materia> materias, IMontadorDeGradeCallback callback) {

        gradesMontadas = new LinkedList<>();

        materiasSelecionadas = materias;

        this.callback = callback;

        //ordenar matérias por prioridade
        Collections.sort(materiasSelecionadas, new Comparator<Materia>() {
            @Override public int compare(Materia m1, Materia m2) {
                return m1.getPrioridade() - m2.getPrioridade(); // Ascending
            }

        });
    }

    public List<Grade> getGradesMontadas() {
        return gradesMontadas;
    }


    private String converterParaCaminho(int posicaoMateria, int posicaoTurma) {

        return "M" + posicaoMateria + "T" + posicaoTurma;

    }

    //retorna uma matéria que contem apenas a turma especificada no caminho
    private Materia puxarMateriaDoCaminho(String caminho) {

        //Caminho:

        //[0] = sempre será M
        //[1...n] = int que representa Materia
        //[n+1] = sempre será T
        //[n+2...FIM] = int que representa Turma


        //vamos separar a string caminho em suas partes
        String parteMateria = caminho.substring(1, caminho.indexOf('T')); //pega até T
        String parteTurma = caminho.substring(caminho.indexOf('T') + 1); //pega de T até o final


        //convertemos as partes em Integers (tipo evoluido de int)
        //e pegamos os objetos correspondentes
        Materia m = materiasSelecionadas.get(Integer.parseInt(parteMateria));
        Turma t = m.getTurmas().get(Integer.parseInt(parteTurma));

        //cria uma copia da materia e da turma para não ter nenhuma alteração no objeto original
        Materia copiaMateria = new Materia(m.getNome(), m.getCodigo(), m.getCreditos());
        Turma copiaTurma = new Turma(t.getIdentificadorDaTurma(), t.getProfessor(), t.getAulas(), t.getCampus());

        //adiciona a turma escolhida como a unica turma da materia
        copiaMateria.adicionarTurma(copiaTurma);

        //retornamos a matéria
        return copiaMateria;
    }


    @Nullable
    private void criarGrade() {
        //passos:
        //1 - fixar uma matéria A
        //2 - fixar uma turma da matéria A
        //3 - Pegar a próxima matéria e salvar em matéria B
        //4 - se não houver próxima matéria então gerar grade, se houver entao continuar
        //5 - pegar uma matéria X
        //6 - fixar turma da matéria X
        //7- se não houver conflitos adicionar a turma, caso contrário passa pra próxima turma
        //8 - repetir passos 5,6,7 para todas as materias
        // 9 - montar a grade
        //10- fixar materia A+1 e repetir os passos processo ate que se acabem as turmas de A

        //a string caminho representa o que foi fixo até aqui
        //EX: M1T2 quer dizer que materiasSelecionadas.get(1).getturmas().get(2)

        //formatação MiTj,MpTq...,MwTz; (organizado em uma lista

        //se não tem matérias, não tem nada, podemos acabar aqui
        if(materiasSelecionadas == null || materiasSelecionadas.size() < 1){
            Grade g = new Grade();

            gradesMontadas.add(g);

            callback.gradeMontada();

            return;
        }

        //fixando a primeira máteria
        int temp = 0; // armazena temporariamente o numero de turmas de uma matéria
        maior = 0; // vai receber o index da matéria com mais turmas
        for(int mat = 0; mat < materiasSelecionadas.size(); mat++ ){
            int tamanho = materiasSelecionadas.get(mat).getTurmas().size();
            if (tamanho > temp ){
                temp = tamanho;
                maior = mat;
            }
        }
        LinkedList<Turma> turmasA = materiasSelecionadas.get(maior).getTurmas();

        //pegando cada turma da primeira matéria
        for (int i = 0; i < turmasA.size(); i++) {
            //Redefine meu caminho sempre que começo uma nova árvore
            LinkedList<String> caminhoAtual;
            caminhoAtual = new LinkedList<>();

            //vou fixar uma turma "i"
            //vou verificar se existe uma próxima matéria...
            if (materiasSelecionadas.size() == 1) {
                //...se não
                //não tem proxima matéria, então vamos pegar a turma de agora e criar uma grade somente com ela
                //para isso, vamos adicionar a turma ao caminho que usamos para chegarmos aqui
                caminhoAtual.add(converterParaCaminho(maior, i));
                //e gerar a grade com o caminhoAtual
                montarGrade(caminhoAtual);


            } else {
                //...se sim, eu vou pegar as turmas das matérias seguintes
                caminhoAtual.add(converterParaCaminho(maior, i));

                if(maior != 0){
                    //se a matéria inicial não for a de index zero, vou começar do zero
                    pegarMatériaSeguinte(caminhoAtual, 0);
                } else {
                    // se for a de index zero eu pego a seguinte
                    pegarMatériaSeguinte(caminhoAtual, maior+1);
                }

            }

        }
        callback.gradeMontada();
    }


    private boolean temConflito(LinkedList<String> caminhoAtual, LinkedList <Turma> turmas, int index_turma ){

        boolean temConflito;

        for (String s : caminhoAtual) {

            Materia m = puxarMateriaDoCaminho(s);
            temConflito = Turma.temConflito(m.getTurmas().getFirst(), turmas.get(index_turma));

            if(temConflito){

                String caminho = "";

                for (String parte: caminhoAtual) {
                    caminho += parte;
                }

                Horario horariosTurma[] = turmas.get(index_turma).getAulas();

                String horariosConflitantes = "";

                for (Horario horario : horariosTurma) {

                    horariosConflitantes+= horario.getDia_STR() + " - " + horario.getInicio_STR() + " - " + horario.getFim_STR() + "\n";

                }

                Log.i("MONTADORDEGRADE", "Tem conflito entre o caminho: " + caminho +
                        "\n Materia do caminho " + m.getNome() + " (turma: "+ m.getTurmas().getFirst().getIdentificadorDaTurma()+")"
                        +"\n Turma conflitante: "  + turmas.get(index_turma).getIdentificadorDaTurma() + " Prof: " + turmas.get(index_turma).getProfessor()
                        +"\n Horarios conflitantes: " + horariosConflitantes
                );
                return true;
            }

        }



        return false;
    }

    private void pegarMatériaSeguinte( LinkedList<String> caminhoAtual, int index_materia){

        LinkedList<Turma> turmasX = materiasSelecionadas.get(index_materia).getTurmas();
        //fixamos uma turma em turmasX
        for (int j = 0; j < turmasX.size(); j++) {
            //agora vamos verificar se há um conflito entre a turma X (que é a turma que estamos querendo por)
            //e as turmas já escolhidas no caminho atual
            if (temConflito(caminhoAtual, turmasX, j)) {
                //se há um conflito...
                if(j == turmasX.size() - 1){
                    //... e é a última turma...
                    if(index_materia < materiasSelecionadas.size()-1 ){
                        //... mas não a última matéria
                        pegarMatériaSeguinte(caminhoAtual, index_materia +1);
                        break;
                    } else {
                        //... e a última matéria
                        montarGrade(caminhoAtual);
                        break;
                    }
                }
                    //... se não for a última turma, então pegar turma seguinte

            } else {
                //se não tem conflito, vamos adicionar a turma ao caminho...
                if(index_materia != maior){
                    // ...caso ela não seja a matéria fixada
                    caminhoAtual.add(converterParaCaminho(index_materia, j));
                }
                //agora vamos montar grade ou pegar próxima matéria
                if(index_materia < materiasSelecionadas.size()-1 ){
                    //...se não for a última matéria eu faço o mesmo procedimento com a matéria seguinte
                    pegarMatériaSeguinte(caminhoAtual, index_materia +1);
                    break;
                } else {
                    //... se for a última matéria eu monto a grade
                    montarGrade(caminhoAtual);
                    break;
                }

            }
        }

    }




    private void montarGrade(LinkedList<String> caminho) {

        Grade g = new Grade();

        String cam = "";

        for (String s : caminho) {

            cam += s;
            g.adicionarMateria(puxarMateriaDoCaminho(s));
        }

        Log.i("GRADEMONTADA", "Grade montada com o caminho: " + cam);


        gradesMontadas.add(g);


    }


    @Override
    public void run() {

        criarGrade();
    }

}