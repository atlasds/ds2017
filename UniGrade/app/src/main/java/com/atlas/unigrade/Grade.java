package com.atlas.unigrade;

import java.util.LinkedList;



/**
 * Classe Grade:
 * Responsável por representar uma grade, ela tem a estrutura de dados
 */

public class Grade {

    private LinkedList<Materia> materias;

    Grade(){
	//Criação de uma LinkedList para matérias
        materias = new LinkedList<>(); 
    }

    void adicionarMateria(Materia paraAdicionar){ 
	//Se matéria for igual a "vazio", sempre testar a condição para criar uma nova LinkedList
        if(this.materias == null){ 
            materias = new LinkedList<>();
        }
	//Adiciona matérias à LinkedList
        this.materias.add(paraAdicionar); 
    }
    //Getter para receber matérias elencadas
    LinkedList<Materia> getMaterias(){ 

        return this.materias;
    }



}
