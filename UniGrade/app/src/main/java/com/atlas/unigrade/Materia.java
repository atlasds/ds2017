package com.atlas.unigrade;

import java.util.List;
import java.security.InvalidParameterException;
import java.util.LinkedList;

public class Materia {
	
	private LinkedList<Turma> turmas;
	//uso:
	//turmas.get(index) retorna um objeto do tipo turma
	//turmas.add(Turma t) adiciona uma turma a lista
	//precisa ser inicalizada
	
	private String nome;
	private String codigo;
	
	//segue o padrao UnB (00-00-00-00)
	private String creditos;
	
	private int prioridade = 1;
	private int aulasSemanais;
	//Será utilizado pela classe Grade para travar uma turma enquanto itera pela turma das outras disciplinas
	private int turmaEscolhida = -1;
	
	Materia(String nome , String codigo, String creditos, int aulasSemanais){
		
		this.codigo = codigo;
		this.nome = nome;
		this.creditos = creditos;
		this.aulasSemanais = aulasSemanais;
		
		//inicializa a lista deste obj como uma LinkedList
		//essa lista eh ordenada pela ordem de entrada de dados
		turmas = new LinkedList<>();
		
		return;
	}
	
	
	Materia(String nome , String codigo, String creditos){
		
		this.codigo = codigo;
		this.nome = nome;
		this.creditos = creditos;
		
		//inicializa a lista deste obj como uma LinkedList
		//essa lista eh ordenada pela ordem de entrada de dados
		turmas = new LinkedList<>();
		
		return;
	}
	
	Materia(String nome , String codigo){
		
		this.codigo = codigo;
		this.nome = nome;
		
		//inicializa a lista deste obj como uma LinkedList
		//essa lista eh ordenada pela ordem de entrada de dados
		turmas = new LinkedList<>();
		
		return;
	}
	
	/***
	 * Cria uma materia a partir de um nome no formato codMat ou matCod
	 * */
	Materia(String nome){
		this.interpretarNome(nome);
		
		//inicializa a lista deste obj como uma LinkedList
		//essa lista eh ordenada pela ordem de entrada de dados
		turmas = new LinkedList<>();
		
		return;
	}
	
	/***
	 * Esta funcao trata o nome, verificando se ele eh cod - Mat ou mat - Cod
	 * */
	private void interpretarNome(String nome) {
		
		//se a string estiver vazia, jogar erro
		if(nome.isEmpty()) {
			throw new InvalidParameterException();
		}else {
			
			//se o primeiro caractere for um numero, entao ele supoe que seja no formato cod - mat
			if("0123456789".contains(nome.substring(0, 1))) {
				
				//a primeira parte que o programa recebe eh o codigo
				this.codigo = nome.substring(0, nome.indexOf('-'));
				this.nome = nome.substring(nome.indexOf('-') + 1);
			}else {
				//senao, o programa recebe o formato mat - cod
				this.codigo = nome.substring(nome.indexOf('-') + 1);
				this.nome = nome.substring(0, nome.indexOf('-'));
			}
		}
		
		return;		
	}

	/***
     * Retorna a string nomeCompleto (do tipo: cod - Mat, mat - Cod, mat, cod) em uma array de
     * tamanho 2 em que os indices representam:
     * 0 = cod
     * 1 = nome
     * */
	public static String[] interpretaCodMat(String nomeCompleto) {
		
		//0 = cod
		//1 = nome
		String[] retorno = new String[2];
		
		//verifica se e um nome cod - mat ou so cod ou so mat
		if(nomeCompleto.contains("-") == false) {
			
			if("0123456789".contains(nomeCompleto.substring(0, 1))) {
				retorno[0] = nomeCompleto;
				retorno[1] = null;
			}else {
				retorno[0] = null;
				retorno[1] = nomeCompleto;
			}
			
		//se for um cod - mat ou mat - cod
		}else if("0123456789".contains(nomeCompleto.substring(0, 1))) {
			//se o primeiro caractere for um numero
			
			//a primeira parte eh o codigo
			retorno[0] = nomeCompleto.substring(0, nomeCompleto.indexOf('-'));
			retorno[1] = nomeCompleto.substring(nomeCompleto.indexOf('-') + 1);
		}else {
			
			retorno[0] = nomeCompleto.substring(nomeCompleto.indexOf('-') + 1);
			retorno[1] = nomeCompleto.substring(0, nomeCompleto.indexOf('-'));
		}
		
		return retorno;
	}
	//um setter que coloca o valor de creditos para cada materia
	public void setCreditos(String creditos) {
		this.creditos = creditos;
	}
	//recebe uma turma como parametro e adiciona ela ao objeto materia
	void adicionarTurma(Turma t) {
		this.turmas.add(t);
		
		return;
	}
	//remove a turma do objeto materia
	void removerTurma(Turma remover){

		for(Turma t : this.turmas){
			if(t.getIdentificadorDaTurma().compareTo(remover.getIdentificadorDaTurma()) == 0){

				this.turmas.remove(t);
				break;
			}
		}

	}
	//Getter em formato de linkedlist para retornar as turmas
	LinkedList<Turma> getTurmas(){
		return this.turmas;
	}
	//Getter que retorna nome como uma string
	String getNome() {
		return this.nome;
	}
	//Getter que retorna codigo como uma string
	public String getCodigo() {
		return codigo;
	}
	//Getter que retorna o valor de creditos como um string (00-00-00-00)
	String getCreditos() {
		return this.creditos;
	}
	//Getter para o formato CodMat, que retorna String de Codigo e Materia para o objeto
	public String getCodMat() {
		return this.getCodigo() + "-" + this.getNome();
	}
	//Getter para o formato MatCod, que retorna String de Materia e Codigo para o objeto
	public String getMatCod() { return  this.getNome() + "-" + this.getCodigo();
	}
	//Retorna prioridade da materia (escolhida pelo usuario)
	public int getPrioridade() {
		return prioridade;
	}
	//Recebe prioridade e aloca ela ao objeto
	public void setPrioridade(int prioridade) {
		this.prioridade = prioridade;
	}
	//Retorna o numero de aulas semanais de cada materia
	public int getAulasSemanais() {
		return aulasSemanais;
	}
	//Recebe o numero de aulas e aloca elas ao objeto
	public void setAulasSemanais(int aulasSemanais) {
		this.aulasSemanais = aulasSemanais;
	}
	// Parte que recebe e aloca o valor da turma escolhida ao objeto.
	public void setTurmaEscolhida(int turmaEscolhida) { this.turmaEscolhida = turmaEscolhida; }

	public int getIntTurmaEscolhida(){ return turmaEscolhida; }
	
	public Turma getTurmaEscolhida(){

		return this.turmas.get(turmaEscolhida);

	}


}
